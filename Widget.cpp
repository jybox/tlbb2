#include "Widget.h"
#include "ui_Widget.h"

Widget::Widget():ui(new Ui::Widget)
{
    ui->setupUi(this);

    QString serverInfo = getHttpContent("http://jybox.net/217-tlbb2-switch");

    if(serverInfo.indexOf("TLBB2OK") == -1)
    {
        QMessageBox::critical(this, tr("无法使用"), tr("请联系作者"));
        qApp->quit();
        exit(0);
    }


    connect(ui->getH1, SIGNAL(clicked()), this, SLOT(getHwnd()));
    connect(ui->getH2, SIGNAL(clicked()), this, SLOT(getHwnd()));
    connect(ui->getH3, SIGNAL(clicked()), this, SLOT(getHwnd()));
    connect(ui->getH4, SIGNAL(clicked()), this, SLOT(getHwnd()));
    connect(ui->getH5, SIGNAL(clicked()), this, SLOT(getHwnd()));
    connect(ui->getH6, SIGNAL(clicked()), this, SLOT(getHwnd()));
    connect(ui->getH7, SIGNAL(clicked()), this, SLOT(getHwnd()));
    connect(ui->getH8, SIGNAL(clicked()), this, SLOT(getHwnd()));
    connect(ui->getH9, SIGNAL(clicked()), this, SLOT(getHwnd()));

    timer.setSingleShot(false);
    timer.setInterval(loopTime);
    timer.start();

    connect(&timer,SIGNAL(timeout()),this,SLOT(mainLoop()));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::mainLoop()
{
    for(int i=1; i<10; i++)
    {
        QCheckBox* check = this->findChild<QCheckBox*>(QString("check%1").arg(i));
        if(check->isChecked())
        {
            HWND hwnd = (HWND)this->findChild<QPushButton*>(QString("getH%1").arg(i))->text().toInt();

            if(!hwnd)
            {
                check->setChecked(false);
                continue;
            }

            sleep(delayTime);
            keyPress(hwnd, VK_F9);
            sleep(delayTime);
            keyPress(hwnd, VK_F1);

            keyPress(hwnd, VK_F2);
            keyPress(hwnd, VK_F3);
            keyPress(hwnd, VK_F4);
            keyPress(hwnd, VK_F5);
            keyPress(hwnd, VK_F6);
            keyPress(hwnd, VK_F7);
        }
    }
}

void Widget::sleep(int ms)
{
    QTimer waiter;
    waiter.setInterval(ms);
    QEventLoop wait;
    waiter.start();
    QObject::connect(&waiter, SIGNAL(timeout()), &wait, SLOT(quit()));
    wait.exec();
}

void Widget::keyPress(HWND hwnd, int key)
{
    ::PostMessage(hwnd, WM_KEYDOWN, key, 0);
    sleep(delayTime);
    ::PostMessage(hwnd, WM_KEYUP, key, 0);
    sleep(delayTime);
}

QString Widget::getHttpContent(QString url)
{
    if(!manager)
        manager = new QNetworkAccessManager;

    QNetworkRequest request;
    request.setUrl(QUrl(url));
    QNetworkReply *reply = this->manager->get(request);

    QEventLoop waitDownload;
    QObject::connect(reply, SIGNAL(finished()), &waitDownload, SLOT(quit()));
    waitDownload.exec();

    return QString::fromUtf8(reply->readAll());
}

void Widget::getHwnd()
{
    sleep(2000);
    QPushButton* getH = qobject_cast<QPushButton*>(QObject::sender());
    getH->setText(QString::number((int)::GetForegroundWindow()));
    QMessageBox::information(this, tr("成功"), tr("成功"));
}

void Widget::on_isTop_clicked(bool checked)
{
    hide();
    if(checked)
    {
        f = windowFlags();
        setWindowFlags(Qt::WindowStaysOnTopHint);
    }
    else
    {
        setWindowFlags(f);
    }
    show();
}

void Widget::on_checkAll_clicked(bool checked)
{
    for(int i=1; i<10; i++)
    {
        QCheckBox* check = this->findChild<QCheckBox*>(QString("check%1").arg(i));
        check->setChecked(checked);
    }
}

void Widget::on_about_clicked()
{
    QMessageBox::information(this, tr("备注"), tr("选择目标：F9\n普通攻击：F1\n技能：F2-F7\n操作延迟：%1\n循环间隔：%2").arg(delayTime).arg(loopTime));
}
