#-------------------------------------------------
#
# Project created by QtCreator 2013-03-09T18:25:21
#
#-------------------------------------------------

QT       += core gui widgets network

CONFIG += c++11

TARGET = TLBB2
TEMPLATE = app

SOURCES += main.cpp\
        Widget.cpp

HEADERS  += Widget.h

FORMS    += Widget.ui
