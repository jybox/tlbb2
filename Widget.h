#ifndef WIDGET_H
#define WIDGET_H

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QtNetwork>

namespace Ui
{

class Widget;

}

const int loopTime = 10;
const int delayTime = 5;

class Widget : public QWidget
{
    Q_OBJECT
    
public:
    explicit Widget();
    ~Widget();

public slots:
    void getHwnd();
    
private slots:
    void mainLoop();
    void on_isTop_clicked(bool checked);
    void on_checkAll_clicked(bool checked);
    void on_about_clicked();

private:
    void sleep(int ms);
    void keyPress(HWND hwnd, int key);
    QString getHttpContent(QString url);

    Ui::Widget *ui;
    QTimer timer;

    QNetworkAccessManager *manager = nullptr;
    Qt::WindowFlags f;
};

#endif // WIDGET_H
